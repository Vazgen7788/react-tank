import { combineReducers } from 'redux';

import tanks from './tanks';

export default combineReducers({
  tanks
});
