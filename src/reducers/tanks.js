import * as types from '../constants/ActionTypes';

const initialState = [];

const carBrands = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_TANK:
      return [
        ...state,
        action.tank
      ];
    default:
      return state;
  }
};

export default carBrands;
