import React, { Component } from 'react';
import './App.css';
import Board from './containers/Board';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">React Tanks</h1>
        </header>
        <Board />
      </div>
    );
  }
}

export default App;
