import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addTank } from 'actions/tankActions';
import shortid from 'shortid';

import Tank from 'components/Tank';
import './index.css';

export const BOARD_WIDTH = 1024;
export const BOARD_HEIGHT = 600;

class Board extends Component {
	componentWillMount() {
    const { addTank } = this.props;
    addTank({
      id: shortid.generate(),
      posX: BOARD_WIDTH / 2,
      posY: BOARD_HEIGHT / 2,
      tDir: 'RIGHT',
      cDir: 'RIGHT',
      HP: 100
    });
	}

	outBounds(x,y) {
		return ( x < 0 || y < 0 || x > BOARD_WIDTH || y > BOARD_HEIGHT );
	}

	render() {
    const { tanks } = this.props;
		return (
			<div className="game-board">
        {tanks.map((t) => {
          return (
            <Tank tank={t} />
          );
        })}
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
  tanks: state.tanks
});

export default connect(mapStateToProps, {
  addTank
})(Board);
