export const ADD_TANK = 'ADD_TANK';

export const CHANGE_TANK_POSITION_UP = 'CHANGE_TANK_POSITION_UP';
export const CHANGE_TANK_POSITION_RIGHT = 'CHANGE_TANK_POSITION_RIGHT';
export const CHANGE_TANK_POSITION_DOWN = 'CHANGE_TANK_POSITION_DOWN';
export const CHANGE_TANK_POSITION_LEFT = 'CHANGE_TANK_POSITION_LEFT';

export const CHANGE_CANNON_DIRECTION_UP = 'CHANGE_CANNON_DIRECTION_UP';
export const CHANGE_CANNON_DIRECTION_RIGHT = 'CHANGE_CANNON_DIRECTION_RIGHT';
export const CHANGE_CANNON_DIRECTION_DOWN = 'CHANGE_CANNON_DIRECTION_DOWN';
export const CHANGE_CANNON_DIRECTION_LEFT = 'CHANGE_CANNON_DIRECTION_LEFT';
