import shortid from 'shortid';
import * as types from 'constants/ActionTypes';

export const addTank = ({ id, posX, posY, tDir, cDir, HP }) => dispatch => {
  dispatch({
    type: types.ADD_TANK,
    tank: {
      id,
      posX,
      posY,
      tDir,
      cDir,
      HP
    }
  });
};
